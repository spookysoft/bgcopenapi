# Background Screening API v1.0
***
***

An open web API provided by SterlingBackcheck to allow clients to interact with Sterling background check systems without having to setup a custom integration. This API will give developers the ability to submit background screenings and query for their results.

## Background Check Service
Sterling open API allows clients to place background screenings with as little or as much candidate information as needed for the screen. By utilizing Sterling "Screening Direct" in the background screening process, screens can be submitted with as little candidate information passed as first name, last name, and email address. 

What this means for the developers is that after a screening is submitted to the API, an email is sent to the candidate with a link to Sterling Screening Direct. When the candidate follows the link to Screening Direct in their email, they fill out any remaining information needed for the screen on Screening Direct's secure web form. If more data than the bare minimum is passed in the API call, it will also appear in Screening Direct so that the candidate does not have to enter it again.

### Packages
As part of the Open API, Sterling has provisioned a common set of packages that include commonly requested background check services. When submitting a background check to the API, the request must include the a package ID from this common set. Requests without a package id will not be handled and will return an error.

#### Available Background Screening Packages
_Subject to change*_

|Name | Code | Services|
|---------|---------|------------|
| Standard | 28404 | SSV1, CRFM, CRST, CRNATVNS|
| Standard + Employment | 28406 | SSV1, CRFM, CRST, CRNATVNS, VCEMP|
| Standard + Driver | 24808 | SSV1, CRFM, CRST, CRNATVNS, DR, OFAC, CRSEXDOJ|


## Tennant Provisioning
_TBD_

# API
***
***
## Connectivity
### Authorization
Developers interacting with Sterling OpenAPI will be required to pass an authentication token with each request in the http header. This token will be an HTTP Bearer authentication token provided when the developing organization is provisioned with an API tennant. In the future, they may be support for other authentication methods such as OAuth2.0. 

####Example HTTP Basic Authentication Header Line
	Authorization: Bearer c29tZXVzZXJAc29tZXRoaW5nLmNvbTpzb21lcGFzcw==

#### Base URL
The base URL to be used with all Sterling OpenAPI calls

	https://api.sterlingbackcheck.com
	
### Security
All API calls made to Sterling Open API require HTTPS (TLS) connections. All internal communications between Sterling Systems also run on HTTPS. No PII (personal identifier information) will persist in the Sterling API layer.

***
***

## Resources

The following resources in this section will be the key operators when accessing the API.

### /screenings
The screenings API resource with no screening identifier (orderId) allows developers to post new screening requests.

#### URI

	https://api.sterlingbackcheck.com/screenings

#### Supported Operations

	HTTP POST

#### Request Headers

	Content-Type: application/json; charset=utf-8
	Sterling-Account: stringAccountValue
	Sterling-Organization: billingcode
	Sterling-Credentials: base64 encoded user/pw



#### Request
Sample basic request payload to submit a background screening. Please see data formats section of appendix to see an expanded data structure with more candidate data fields and an explanation of each element.

```json
{
  "referenceId": "developer-candidate-guid-001",
  "packageId": "10000",
  "personalData": {
    "firstName": "John",
    "lastName": "Doe",
    "email": "some1@something.com"
  }
}
```

***

#### Success

| HTTP Status Code| Message | Description |
|------------------|-------------|-----------------|
| 202 | Accepted | Request received successfully |


After submitting your request payload to the /screenings resource, if everything checked out good (data format and data validation passes) You'll recieve the following response back. 

**Response Headers**

	Cache-Control: no-cache
	Location: https://api.sterlingbackcheck.com/screenings/SL1440795627092API
	Date: 2015-08-28T21:00:27.092Z

**Response**

```json
{
    "referenceId": "developer-candidate-guid-001",
    "orderId":"SL1440795627092API",
    "status": "Drafted"
}
```

The header will have these special headers for screening information

**Location** - The URL link to view the status of the successfully submitted screening.

**Date** - UTC Timestamp of the datetime the request was processed.

In the payload there will be the following elements

**referenceId** - The same value in $referenceId in the screening payload and is usually reserved for the candidate guid in the developer's system.

**orderId** - Reference for the screening in Sterling's internal system. Can be used to query API for screening status.

**status** - The status of the submitted screening.

***

#### Error

| HTTP Status Code| Message | Description |
|------------------|-------------|-----------------|
| 400 | Bad Request | See data errors contained from response payload |
| 401 | Unauthorized | Request received, but the authentication token used was incorrect, nonexistant, or expired |
| 404 | Not Found | Please check request URI, resource not found or not yet provisioned |
| 406 | Unacceptable | Please check request to ensure the message data type is JSON |
| 500 | Internal Server Error | Request handler failed unexpectedely, please check data format and resend request |

After submitting your request payload, if there is a problem with the data format or the data sent is invalid, then one would expect to get something like this back synchronously.

**Response Headers**
  
	Cache-Control: no-cache
	Date: 2015-08-28T21:00:27.092Z

**Response**

```json
  [
    {
        "message":"Package Id not found in request",
        "type": "User",
        "resolution": "Resend request with package id"
    }
  ]
```

**errors** - A JSON array of all of the data validtion and formatting errors found in the screening request. If there were multiple errors, there will be multiple array elements.

**message** - An element contained in an array element of the "errors" array. This string value will indicate a single error that was encountered.

**type** - Type of error that occured, "User" or "System".

**resolution** - Recommended action to take in order to resolve the issue indicated by the message

***
***

### /screenings/{orderId}
The screenings API resource with screening identifier will allow developers to get the status of their already placed screenings by utilizing an already existing order id from a successful screening request.

#### URI
	https://api.sterlingbackcheck.com/screenings/SL1440795627092API

#### Supported Operations
	HTTP GET
	
#### Request Headers

	Cache-Control: no-cache
	Sterling-Account: stringAccountValue
	Sterling-Organization: billingcode
	Sterling-Credentials: base64 encoded user/pw


#### Request
	NA

***
#### Success

| HTTP Status Code| Message | Description |
|------------------|-------------|-----------------|
| 200 | OK | Request received successfully |


After submitting a request to /screenings/{orderId}, if everything checks out ok (ex. orderId exists in our system) the following response will be returned synchronously. 

**Response Headers**
  
	Cache-Control: no-cache
	Date: 2015-08-28T21:00:27.092Z

**Response**

```json
{
    "referenceId": "developer-candidate-guid-001",
    "orderId":"SL1440795627092API",
    "status": "Completed",
    "score": "Clear",
    "report": "https://qasecure.sterlingdirect.com/sys/OneClick.aspx?METHOD=BGCHECK_PRTREPORT&;CUSTID=YEo1kHnc%7cgzLuoEghgh32sV4TA%3d%3d&;BGORDERID=r324dfdsfasdfsdfsd"
}
```

**referenceId** - The same value in $referenceId in the screening payload and success response. It is usually reserved for the candidate guid in the developer's system.

**orderId** - Reference for the screening in Sterling's internal system.

**status** - The status of the requested screening.

**score** - The score for the requested screening. This element will not be returned until the screening status has a value of "Completed".

**report** - The https link to the full report for the screening in Sterling's system. This element will not be returned until the screening status has a value of "Completed".

***

#### Error

| HTTP Status Code| Message | Description |
|------------------|-------------|-----------------|
| 400 | Bad Request | See data errors contained from response payload |
| 401 | Unauthorized | Request received, but the authentication token used was incorrect, nonexistant, or expired |
| 404 | Not Found | Please check request URI, resource not found or not yet provisioned |
| 500 | Internal Server Error | Request handler failed unexpectedely, please check data format and resend request |

If there was an issue with a request to /screenings/{orderId} the following response will be returned synchronously.

**Response Headers**
  
	Cache-Control: no-cache
	Date: 2015-08-28T21:00:27.092Z

**Response**

```json
  [
    {
        "message":"Order SL1440795627092API not found",
        "type":"User",
        "resolution": "Check order id and resubmit request"
    }
  ]
```

**errors** - A JSON array of all of the data validtion and formatting errors found in the screening request. If there were multiple errors, there will be multiple array elements.

**message** - An element contained in an array element of the "errors" array. This string value will indicate a single error that was encountered.

**type** - Type of error that occured, "User" or "System".

**resolution** - Recommended action to take in order to resolve the issue indicated by the message

***
***

# Appendix

***
***

## Data Formats

### Common Data

The common block of data that can accompany any request to /screenings

```json
{
  "referenceId": "developer-candidate-guid-001",
  "packageId": "10000",
  "personalData": {
    "firstName": "John",
    "middleName": "Anthony",
    "lastName": "Doe",
    "dateOfBirth": "1975-06-01",
    "gender": "M",
    "email": "some1@something.com",
    "phone": "215-555-555",
    "addresses": [
      {
        "isPrimary": true,
        "country": "US",
        "address": "123 Vine Ave",
        "city": "New York",
        "region": "New York",
        "postalCode": "10001"
      }
    ],
    "personalIds": [
      {
        "jurisdiction": "US",
        "type": "SSN",
        "idValue": "889-74-6887"
      },
      {
        "jurisdiction": "US/NY",
        "type": "DMV",
        "idValue": "111111111"
      }
    ]
  }
}
```

**Reference**

| Element Name | Description | Data Format | JSONPath |
|---|---|---|---|
| referenceId | Globally unique value reserved for client's internal system candidate identifier | Text | ``` $referenceId ```|
| packageId | Sterling background screening package identifier | Text, Must correspond to a code in the "Available Screening Packages" section of this guide | ``` $packageId ``` |
| firstName | Candidate First Name | Text | ``` $personalData.firstName ```|
| middleName | Candidate Middle Name | Text | ``` $personalData.middleName ```|
| lastName | Candidate Last Name | Text | ``` $personalData.lastName ```|
| dateOfBirth | Candidate date of birth | Text, (candidate must be at least 18 years of age), yyyy-mm-dd | ``` $personalData.dateOfBirth ``` |
| gender | Candidate gender (Unknown, Male, Female) | Text, one of (U, M, F) | ``` $personalData.gender ``` |
| email | Candidate Email Address | Text, Email Format (Must contain @ and .extension) | ``` $personalData.email ``` |
| phone | Candidate Phone Number | Text, No international code, - after first and second set of digits (222-222-2222) | ``` $personalData.phone ``` |
| isPrimary | Flag setting a postal address array element as the candidate's primary address | boolean | ``` $personalData.addresses[*].isPrimary ``` |
| country | Candidate address country code | Text, ISO 3166-1 alpha2 (Ex: US) | ``` $personalData.addresses[*].country ``` |
| address | Candidate street / apt | Text | ``` $personalData.addresses[*].addressLine ```|
| city | Candidate address city | Text | ``` $personalData.addresses[*].city ``` |
| region | Candidate address state / province | Text | ``` $personalData.addresses[*].region ``` |
| postalCode | Candidate address post code | Text | ``` $personalData.addresses[*].postalCode ``` |
| jurisdiction | Candidate government id country code for social identifiers or country code/state code that issued the id for driving licenses. | Text, ISO 3166-1 alpha2 country code (Ex: US) or (US/NY)| ``` $personalIds[*].jurisdiction ``` |
| type | Indicates type of candidate government id array element (SSN/SIN, DMV (drivers license)) | Text, Limited to (SSN,SIN,DMV) | ``` $personalIds[*].type ``` |
| idValue | Element containing the actual id in the candidate government id array element | Text, (if SSN/SIN must be of format 001-01-0001) | ``` $personalIds[*].idValue ```|

###Education Screening Data
Data section for a request in the case an education screen is part of the screening package sent and the client wants to send more than just basic data. This section would be included screeningData section of the request payload.

```json
{
  "education": [
    {
      "schoolName": "San Jose University",
      "city": "Denver",
      "region": "Colorado",
      "country": "US",
      "type": "University",
      "major": "Applied Mathematics",
      "degreeType": "BS",
      "degreeComplete": "Yes",
      "startDate": "2002-10-01",
      "endDate": "2006-08-01"
    }
  ]
}
```

**Reference**

| Element Name | Description | Data Format | JSONPath |
|---|---|---|---|
| schoolName | Candiate's school name | Text | ``` $screeningData.education[*].schoolName ``` |
| city | City of the candidate's school in the current array element | Text | ```  $screeningData.education[*].schoolCity ``` |
| region | State / Province of the candidate's school in the current array element | Text | ```  $screeningData.education[*].schoolRegion ``` |
| country | Country code of the candidate's school in the current array element | Text, ISO 3166-1 alpha2 (Ex: US) | ```  $screeningData.education[*].schoolCountry ``` |
| type | Type of candidate's school in the current array element | Text | ```  $screeningData.education[*].schoolType ``` |
| major | Candidate's major area of study in the current array element | Text | ```  $screeningData.education[*].major ``` |
| degreeType | Name of candidate's degree in the current array element | Text | ```  $screeningData.education[*].degreeType ``` |
| degreeComplete | Indicates whether the degree in the current array element has been completed | Text (Yes/No) | ```  $screeningData.education[*].degreeComplete ``` |
| startDate | Candidate's degree start date | Text | ```  $screeningData.education[*].startDate ``` |
| endDate | Candidate's degree end date | Text | ```  $screeningData.education[*].endDate ``` |





###Employment Screening Data
Data section for a request in the case an employment screen is part of the screening package sent and the client wants to send more than just basic data. This section would be included screeningData section of the request payload.

```json
{
  "employment": [
    {
      "employerName": "American Red Cross",
      "isCurrent": true,
      "positions": [
        {
          "title": "Acting Education Manager",
          "startDate": "2007-01-01",
          "endDate": "2008-01-01",
          "salaryCurrency": "USD",
          "startSalary": 50000,
          "endSalary": 60000,
          "reasonForLeaving": "Commute too long",
          "verificationContacts": [
            {
              "name": "Miles O'Brien",
              "canContact": true,
              "phone": "411-111-1111",
              "email": "someguy@something.com",
              "country": "US",
              "address": "1234 Main Street",
              "city": "New York",
              "region": "NY",
              "postalCode": "10001"
            }
          ]
        }
      ]
    }
  ]
}
```

**Reference**

| Element Name | Description | Data Format | JSONPath |
|---|---|---|---|
| employerName | Candidate's employer name | Text | ``` $screeningData.employment[*].employerName ``` |
| isCurrent | candidate's current employer? | Boolean | ``` $screeningData.employment[*].isCurrent ``` |
| title | Cadidate's job title at a past employer | Text | ``` $screeningData.employment[*].positions[*].title ``` |
| StartDate | Candidate's start date at employer in current position element | Text, (yyyy-mm-dd) | ``` $screeningData.employment[*].positions[*].startDate ``` |
| endDate | Candidate's end date at employer in current position element | Text, (yyyy-mm-dd) | ``` $screeningData.employment[*].positions[*].endDate ``` |
| salaryCurrency | Currency type for candidate's position salary | Text, ISO 4217 (3 character currency code) | ``` $screeningData.employment[*].positions[*].salaryCurrency ``` |
| startSalary | Starting salary for candidate's position | Number, Int | ``` $screeningData.employment[*].positions[*].startSalary ``` |
| endSalary | End salary for candidate's position| Number, Int | ``` $screeningData.employment[*].positions[*].endSalary``` |
| reasonForLeaving | Candidate's reason for leaving past employer | Text | ``` $$screeningData.employment[*].positions[*].reasonForLeaving ``` |
| name | Candidate's verification contact at past employer | Text | ``` $screeningData.employment[*].positions[*].verificationContacts[*].name ``` |
| canContact | Candidate's permission for background check services to contact past employer contact | Boolean | ``` $screeningData.employment[*].positions[*].verificationContacts[*].canContact ``` |
| phone | Candidate's verification contact phone| Boolean | ``` $screeningData.employment[*].positions[*].verificationContacts[*].phone ``` |
| email | Candidate's verification contact email | Boolean | ``` $screeningData.employment[*].positions[*].verificationContacts[*].email ``` |
| country |  Candidate's employer country code | Text, ISO 3166-1 alpha2 (Ex: US) | ```  $screeningData.employment[*].positions[*].verificationContacts[*].country ``` |
| address| Candidate's employer address line | Text | ``` $screeningData.employment[*].positions[*].verificationContacts[*].address ``` |
| city | Candidate's employer city | Text | ``` $screeningData.employment[*].positions[*].verificationContacts[*].city ``` |
| region| Candidate's employer region | Text | ``` $screeningData.employment[*].positions[*].verificationContacts[*].region ``` |
| postalCode | Candidate's employer postal code | Text | ``` $screeningData.employment[*].positions[*].verificationContacts[*].postalCode ``` |



### Full Data
Below is an example of a request payload to /screenings that contains every available section of data. _*Some Sections may not be necessary depending on service breakdown for available packages*_

```json
{
  "referenceId": "developer-candidate-guid-001",
  "packageId": "10000",
  "personalData": {
    "firstName": "John",
    "middleName": "Anthony",
    "lastName": "Doe",
    "dateOfBirth": "1975-06-01",
    "gender":"M",
    "email": "some1@something.com",
    "phone": "215-555-555",
    "addresses": [
      {
        "isPrimary": true,
        "country": "US",
        "address": "123 Vine Ave",
        "city": "New York",
        "region": "NY",
        "postalCode": "10001"
      }
    ],
    "personalIds": [
      {
        "jurisdiction": "US",
        "type": "SSN",
        "idValue": "aaa-bb-cccc"
      },
      {
        "jurisdiction": "US/NY",
        "type": "DMV",
        "idValue": "111111111"
      }
    ]
  },
  "screeningData": {
    "employment": [
      {
        "employerName": "American Red Cross",
        "isCurrent": true,
        "positions": [
          {
            "title": "Acting Education Manager",
            "startDate": "2007-01-01",
            "endDate": "2008-01-01",
            "salaryCurrency": "USD",
            "startSalary": 50000,
            "endSalary": 60000,
            "reasonForLeaving": "Commute too long",
            "verificationContacts": [
              {
                "name": "Miles O'Brien",
                "canContact": true,
                "phone": "411-111-1111",
                "email": "someguy@something.com",
                "country": "US",
                "address": "1234 Main Street",
                "city": "New York",
                "region": "NY",
                "postalCode": "10001"
              }
            ]
          }
        ]
      }
    ],
    "education": [
      {
        "schoolName": "San Jose University",
        "city": "Denver",
        "region": "CO",
        "country": "US",
        "type": "University",
        "major": "Applied Mathematics",
        "degreeType": "BS",
        "degreeComplete": true,
        "startDate": "yyyy-mm-dd",
        "endDate": "yyyy-mm-dd"
      }
    ],
    "license": [
      {
        "nameOnLicense": "John Anthony Doe",
        "licenseNumber": "111111111",
        "agency": "US/NY",
        "startDate": "yyyy-mm-dd",
        "endDate": "yyyy-mm-dd"
      }
    ]
  }
}
```


***
***

## Code Samples

Java Code Samples to perform HTTP POST and GET requests to the API.

### HTTP GET
HTTP Get is what one can use to start an integration that requires no input. Here is a java method that will perform an HTTP GET to a snaplogic URI, (or any URI with basic auth). To use this method you will need the following libraries in your project. Apache HTTPClient 4.x.x +, Apache HTTPCore 4.x.x +, and Apache Commons-Codec 1.9+. No need to handle or setup the input here.

```java
/*This method performs an HTTP Get on a URI with the credentials and parameters provided
 * This method accepts parameters for the URI in string form of param=value separated by ;
 *Ex param1=value;param2=value;param3=value
 *dwhite@snaplogic.com
 */
public void doGet(String uname, String pword, String URI, String params, int followRedirect)
{
    CloseableHttpClient postClient = null;
    CloseableHttpResponse getResponse = null;
    HttpUriRequest getRequest = null;
    BufferedReader responseStream = null;
    String b64Creds = null;
    String inputLine = null;
    String[] tempSplit;
    String[] getParams;


    if(followRedirect == 1)
    {
        //Create a new closeable http client that will follow redirects
        postClient = HttpClientBuilder.create().setRedirectStrategy(new LaxRedirectStrategy()).build();

    }else
    {
        //Create a new closeable http client with default settings
        postClient = HttpClientBuilder.create().build();
    }


    //Create your get request with the given URI
    //Start with a request builder
    RequestBuilder rb = RequestBuilder.get().setUri(URI);

    //Now, split and parse the parameter values
    //First, split the incoming string by ;
    getParams = params.split(";");

    //Iterate over every parameter in our newly formed array and add them to our request builder
    for(String d: getParams)
    {
        //Use the empty temporary array to split the parameter name/value pairs apart
        tempSplit = d.split("=");

        //If there aren't two items in the temporary array (name,value), then we don't wan't anything to do with it
        //go on to the next set or end.
        if(tempSplit.length < 2)
        {
            continue;
        }

        //Finally, add the name/value pairs from the temp array to the request builder
        rb.addParameter(tempSplit[0], tempSplit[1]);
    }


    //Add headers to your requestBuilder, for posting JSON to SnapLogic, we just want authorization

    //Concatenate and base64 encode your credentials for the header
    b64Creds = Base64.encodeBase64String(StringUtils.getBytesUtf8(uname + ":" + pword));

    //Add your authoization header to the request
    rb.addHeader("Authorization", "basic " + b64Creds);


    //Use the request builder to complete forming the request
    getRequest = rb.build();

    //Now setup is done, execute your get with the HttpClient
    try
    {
        getResponse = postClient.execute(getRequest);
    } catch (ClientProtocolException e)
    {
        //handle error
        e.printStackTrace();
    } catch (IOException e)
    {
        //handle error
        e.printStackTrace();
    }

    //Now we need to parse the response
    //Let's start with the headers
    for(Header h: getResponse.getAllHeaders())
    {
        System.out.println(h.getName() + "=" + h.getValue());
    }

    //Now the response data
    try
    {
        //Setup the buffered reader to read from the incoming stream
        responseStream = new BufferedReader(new InputStreamReader(getResponse.getEntity().getContent()));

        //Read the response data line by line
        while ((inputLine = responseStream.readLine()) != null)
        {
            System.out.println(inputLine);
        }

        //Some clean up
        responseStream.close();
        getResponse.close();
        postClient.close();

    } catch (IllegalStateException e)
    {
        //handle error
        e.printStackTrace();
    } catch (IOException e)
    {
        //handle error
        e.printStackTrace();
    }
}
```

### HTTP POST
HTTP Post is what's used to start an integration that requires input. Here is a java method that will can post json data to a snaplogic URI, (or any URI with basic auth). To use this method you will need the following libraries in your project. Apache HTTPClient 4.x.x +, Apache HTTPCore 4.x.x +, and Apache Commons-Codec 1.9+.

```java
/*This method takes a json document as a string and posts it to the given URI with the credentials and parameters provided
 * This method accepts parameters for the URI in string form of param=value separated by ;
 *Ex param1=value;param2=value;param3=value
 *dwhite@snaplogic.com
 */
public void doPost(String uname, String pword, String jsonData, String URI, String params, int followRedirect)
{
    CloseableHttpClient postClient = null;
    CloseableHttpResponse postResponse = null;
    HttpUriRequest postRequest = null;
    BufferedReader responseStream = null;
    StringEntity postData = null;
    String b64Creds = null;
    String inputLine = null;
    String[] tempSplit;
    String[] postParams;


    if(followRedirect == 1)
    {
        //Create a new closeable http client that will follow redirects
        postClient = HttpClientBuilder.create().setRedirectStrategy(new LaxRedirectStrategy()).build();

    }else
    {
        //Create a new closeable http client with default settings
        postClient = HttpClientBuilder.create().build();
    }


    //Create your post request with the given URI
    //Start with a request builder
    RequestBuilder rb = RequestBuilder.post().setUri(URI);

    //Now, split and parse the parameter values
    //First, split the incoming string by ;
    postParams = params.split(";");

    //Iterate over every parameter in our newly formed array and add them to our request builder
    for(String d: postParams)
    {
        //Use the empty temporary array to split the parameter name/value pairs apart
        tempSplit = d.split("=");

        //If there aren't two items in the temporary array (name,value), then we don't wan't anything to do with it
        //go on to the next set or end.
        if(tempSplit.length < 2)
        {
            continue;
        }

        //Finally, add the name/value pairs from the temp array to the request builder
        rb.addParameter(tempSplit[0], tempSplit[1]);
    }


    //Add headers to your requestBuilder, for posting JSON to SnapLogic, we want content type and authorization

    //Concatenate and base64 encode your credentials for the header
    b64Creds = Base64.encodeBase64String(StringUtils.getBytesUtf8(uname + ":" + pword));

    //Add your authoization header to the request
    rb.addHeader("Authorization", "basic " + b64Creds);

    //set your content type header, this would change if you were posting XML instead of JSON, SnapLogic natively operates on JSON.
    rb.addHeader("Content-Type", "application/json");


    //set your post payload, in this case, its a JSON document as a string
    //First create your HttpEntity
    try
    {
        postData = new StringEntity(jsonData);
    } catch (UnsupportedEncodingException e)
    {
        //handle error
        e.printStackTrace();
    }

    //Then set it in the request builder
    rb.setEntity(postData);


    //Use the request builder to complete forming the request
    postRequest = rb.build();

    //Now setup is done, execute your post with the HttpClient
    try
    {
        postResponse = postClient.execute(postRequest);
    } catch (ClientProtocolException e)
    {
        //handle error
        e.printStackTrace();
    } catch (IOException e)
    {
        //handle error
        e.printStackTrace();
    }

    //Now we need to parse the response
    //Let's start with the headers
    for(Header h: postResponse.getAllHeaders())
    {
        System.out.println(h.getName() + "=" + h.getValue());
    }

    //Now the response data
    try
    {
        //Setup the buffered reader to read from the incoming stream
        responseStream = new BufferedReader(new InputStreamReader(postResponse.getEntity().getContent()));

        //Read the response data line by line
        while ((inputLine = responseStream.readLine()) != null)
        {
            System.out.println(inputLine);
        }

        //Some clean up
        responseStream.close();
        postResponse.close();
        postClient.close();

    } catch (IllegalStateException e)
    {
        //handle error
        e.printStackTrace();
    } catch (IOException e)
    {
        //handle error
        e.printStackTrace();
    }
}
```
***
***
