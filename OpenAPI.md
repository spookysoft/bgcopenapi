# Background Check API v1.0
***
***

An open web API provided by SterlingBackcheck to allow clients to interact with Sterling background check systems without having to setup a custom integration. This API will give developers the ability to submit background check orders and query for the results of those background checks.

## Background Check Service
Sterling open API allows clients to place background check orders with as little or as much candidate information as needed for the screen. By utilizing Sterling "Screening Direct" in the background checking process, orders can be submitted with as little candidate information passed as first name, last name, and email address. 

What this means for the developers is that after a background check order is submitted to the API, an email is sent to the candidate with a link to Sterling Screening Direct. When the candidate follows the link to Screening Direct in their email, they fill out any remaining information needed for the screen on Screening Direct's secure web form. If more data than the bare minimum is passed in the API call, it will also appear in Screening Direct so that the candidate does not have to enter it again.

### Packages
As part of the Open API, Sterling has provisioned a common set of packages that include commonly requested background check services. When submitting a background check to the API, the request must include the a package ID from this common set. Requests without a package id will not be handled and will return an error.

#### Available Background Check Packages
_Subject to change*_

|Name | Code | Services|
|---------|---------|------------|
| Basic | 10000 | SSN, EMP|
| Basic Plus | 10001 | SSN, EMP, EDU, CRIM|
| Basic Driver | 10002 | SSN, EMP, MVR, CRIM|
| Basic Criminal | 10003 | SSN, CRIM|

## Tennant Provisioning
_TBD_

# API
***
***
## Connectivity
### Authorization
Developers interacting with Sterling OpenAPI will be required to pass an authentication token with each request in the http header. This token will be an HTTP Basic Authorization token provided when the developing organization is provisioned with an API tennant. In the future, they may be support for other authentication methods such as OAuth2.0. 

####Example HTTP Basic Authentication Header Line
	Authorization: basic c29tZXVzZXJAc29tZXRoaW5nLmNvbTpzb21lcGFzcw==

#### Base URL
The base URL to be used with all Sterling OpenAPI calls

	https://www.sterlingbackcheck.io/api/

There will be an additional section added on per customer that will be provided during onboarding that includes customer and tennant details. The resulting base URL will be something like

	https://www.sterlingbackcheck.io/api/customer/dev/
	
### Security
All API calls made to Sterling Open API require HTTPS (TLS) connections. All internal communications between Sterling Systems also run on HTTPS. No PII (personal identifier information) will persist in the Sterling API layer.

***
***

## Resources

The following resources in this section will be the key operators when accessing the API. Resources are added to your base URL to perform different API operations.


### /order
The order (singlular) API resource will allow developers to post new background check orders for candidates.

#### URI

	https://www.sterlingbackcheck.io/api/customer/dev/order

#### Supported Operations

	HTTP POST

#### Request Headers

	Authorization: basic c29tZXVzZXJAc29tZXRoaW5nLmNvbTpzb21lcGFzcw==
	Content-Type: application/json; charset=utf-8
	
#### Request
Sample basic request payload to submit a background check. Please see data formats section of appendix to see an expanded data structure with more candidate data fields and an explanation of each element.

```json
{
	"BackgroundCheck": {
		"BackgroundSearchPackage": {
			"Screening": {
				"PersonalData": {
					"PersonName": {
					     "GivenName": "Myles",
					     "FamilyName": "O'Brien"
					 },
					 "ContactMethod": {
					 "InternetEmailAddress": "Myles.OB@something.com"
					  }
				},
				"ProcessingInformation": {
					"AccessCredential": {
					    "Account": "124",
					    "Password": "somepassword",
					    "Username": "54701"
					}
				}
			},
			"Screenings": {
				"PackageId": "25286"
			},
			"Organization": {
				"OrganizationId": {
					"IdValue": "somebillingcode"
				}
			}
		},
		"ReferenceId": "SomeClientInternalOrderReference"
	}
}
```

***

#### Success

| HTTP Status Code| Message | Description |
|------------------|-------------|-----------------|
| 200 | Ok | Request received successfully |

After submitting your request payload to the /order resource, if everything checked out good (data format and data validation passes) You'll recieve the following response back. 

**Response**

```json
{
	"ReferenceId": "SomeClientInternalOrderReference",
	"OrderId":"SL1440795627092API",
	"Status": "Drafted",
	"Timestamp": "2015-08-28T21:00:27.092 UTC"
}
```

In the payload there will be the following elements

**ReferenceId** - The same value in $BackgroundCheck.ReferenceId and is usually reserved for the candidate guid in the developer's system.

**OrderId** - Reference for the background check order in Sterling's internal system. Can be used to query API for order status.

**Status** - The status of the submitted background check order.

**Timestamp** - UTC timestamp of when the request was processed.

***

#### Error

| HTTP Status Code| Message | Description |
|------------------|-------------|-----------------|
| 401 | Unauthorized | Request received, but the authentication token used was incorrect, nonexistant, or expired |
| 404 | Not Found | Please check request URI, resource not found or not yet provisioned |
| 415 | Unsupported Media Type | Please check request to ensure the message data type is JSON |
| 500 | Internal Server Error | Request handler failed unexpectedely, please check data format and resend request |

After submitting your request payload, if there is a problem with the data format or the data sent is invalid, then one would expect to get something like this back synchronously.

**Response**

```json
{
  "Errors":
  [
	{
  		"ErrorMessage":"Package Id not found in request",
  		"Path":"$Backgroundcheck.BackgroundSearchPackage.Screenings.PackageId"
	}
  ],
  "Timestamp":"2015-08-28T21:00:27.092 UTC"
}
```

**Errors** - A JSON array of all of the data validtion and formatting errors found in the background check request. If there were multiple errors, there will be multiple array elements. Each element in the Array will contain "ErrorMessage" and 
"Path" elements.

**ErrorMessage** - An element contained in an array element of the "Errors" array. This string value will indicate a single error that was encountered.

**Path** - An element contained in an array element of the "Errors" array. This string value will indicate the JSONPath to the element in the background check which caused the error.

**Timestamp** - UTC timestamp of when the request was processed.

***
***

### /orders
The orders (plural) API resource will allow developers to get the status of their already placed background check orders by utilizing the "OrderId" obtained from posting a successful backgorund check order.

#### URI
	https://www.sterlingbackcheck.io/api/customer/dev/orders/SL1440795627092API

#### Supported Operations
	HTTP GET
	
#### Request Headers
	Authorization: basic c29tZXVzZXJAc29tZXRoaW5nLmNvbTpzb21lcGFzcw==
	
#### Request
	NA

***
#### Success

| HTTP Status Code| Message | Description |
|------------------|-------------|-----------------|
| 200 | Ok | Request received successfully |


After submitting a request to /orders/orderid, if everything checks out ok (ex. OrderId exists in our system) the following response will be returned synchronously. 

**Response**

```json
{
	"ReferenceId": "SomeClientInternalOrderReference",
	"OrderId":"SL1440795627092API",
	"Status": "Completed",
	"Score": "Clear",
	"BgcLink": "https://qasecure.sterlingdirect.com/sys/OneClick.aspx?METHOD=BGCHECK_PRTREPORT&;CUSTID=YEo1kHnc%7cgzLuoEghgh32sV4TA%3d%3d&;BGORDERID=r324dfdsfasdfsdfsd",
	"Timestamp": "2015-08-28T21:00:27.092 UTC"
}
```

**ReferenceId** - The same value in $BackgroundCheck.ReferenceId and is usually reserved for the candidate guid in the developer's system.

**OrderId** - Reference for the background check order in Sterling's internal system.

**Status** - The status of the requested background check order.

**Score** - The score for the requested background check order. This element will contain "NA" until "Status" has a value of "Complete".

**BgcLink** - The https link to the full report for the requested background check in Sterling's system. This element will contain "NA" until "Status" has a value of "Complete".

**Timestamp** - UTC timestamp of when the request was processed.

***

#### Error

| HTTP Status Code| Message | Description |
|------------------|-------------|-----------------|
| 401 | Unauthorized | Request received, but the authentication token used was incorrect, nonexistant, or expired |
| 404 | Not Found | Please check request URI, resource not found or not yet provisioned |
| 500 | Internal Server Error | Request handler failed unexpectedely, please check data format and resend request |

If there was an issue with a request to /orders the following response will be returned synchronously.

**Response**

```json
{
  "Error": {
  		"ErrorMessage":"Order SL1440795627092API not found",
    		"ErrorType":"User",
    		"Resolution": "Check order id and resubmit request"
 },
  "Timestamp":"2015-08-28T21:00:27.092 UTC"
}
```

**Error** - Element containing the details of the error encountered. Contains "ErrorMessage", "ErrorType", "Resolution" elements.

**ErrorMessage** - Human readable interpretation of the error encountered. Subelement of "Error".

**ErrorType** - Contains the error type pointing to the system where the error was encountered. Subelement of "Error".

**Resolution** - Recommended action for resolving the error. Subelement of "Error".

**Timestamp** - UTC Timestamp of when the request was processed.

***
***

# Appendix

***
***
## Code Samples

Java Code Samples to perform HTTP POST and GET requests to the API.

### HTTP GET
HTTP Get is what one can use to start an integration that requires no input. Here is a java method that will perform an HTTP GET to a snaplogic URI, (or any URI with basic auth). To use this method you will need the following libraries in your project. Apache HTTPClient 4.x.x +, Apache HTTPCore 4.x.x +, and Apache Commons-Codec 1.9+. No need to handle or setup the input here.

```java
/*This method performs an HTTP Get on a URI with the credentials and parameters provided
 * This method accepts parameters for the URI in string form of param=value separated by ;
 *Ex param1=value;param2=value;param3=value
 *dwhite@snaplogic.com
 */
public void doGet(String uname, String pword, String URI, String params, int followRedirect)
{
    CloseableHttpClient postClient = null;
    CloseableHttpResponse getResponse = null;
    HttpUriRequest getRequest = null;
    BufferedReader responseStream = null;
    String b64Creds = null;
    String inputLine = null;
    String[] tempSplit;
    String[] getParams;


    if(followRedirect == 1)
    {
        //Create a new closeable http client that will follow redirects
        postClient = HttpClientBuilder.create().setRedirectStrategy(new LaxRedirectStrategy()).build();

    }else
    {
        //Create a new closeable http client with default settings
        postClient = HttpClientBuilder.create().build();
    }


    //Create your get request with the given URI
    //Start with a request builder
    RequestBuilder rb = RequestBuilder.get().setUri(URI);

    //Now, split and parse the parameter values
    //First, split the incoming string by ;
    getParams = params.split(";");

    //Iterate over every parameter in our newly formed array and add them to our request builder
    for(String d: getParams)
    {
        //Use the empty temporary array to split the parameter name/value pairs apart
        tempSplit = d.split("=");

        //If there aren't two items in the temporary array (name,value), then we don't wan't anything to do with it
        //go on to the next set or end.
        if(tempSplit.length < 2)
        {
            continue;
        }

        //Finally, add the name/value pairs from the temp array to the request builder
        rb.addParameter(tempSplit[0], tempSplit[1]);
    }


    //Add headers to your requestBuilder, for posting JSON to SnapLogic, we just want authorization

    //Concatenate and base64 encode your credentials for the header
    b64Creds = Base64.encodeBase64String(StringUtils.getBytesUtf8(uname + ":" + pword));

    //Add your authoization header to the request
    rb.addHeader("Authorization", "basic " + b64Creds);


    //Use the request builder to complete forming the request
    getRequest = rb.build();

    //Now setup is done, execute your get with the HttpClient
    try
    {
        getResponse = postClient.execute(getRequest);
    } catch (ClientProtocolException e)
    {
        //handle error
        e.printStackTrace();
    } catch (IOException e)
    {
        //handle error
        e.printStackTrace();
    }

    //Now we need to parse the response
    //Let's start with the headers
    for(Header h: getResponse.getAllHeaders())
    {
        System.out.println(h.getName() + "=" + h.getValue());
    }

    //Now the response data
    try
    {
        //Setup the buffered reader to read from the incoming stream
        responseStream = new BufferedReader(new InputStreamReader(getResponse.getEntity().getContent()));

        //Read the response data line by line
        while ((inputLine = responseStream.readLine()) != null)
        {
            System.out.println(inputLine);
        }

        //Some clean up
        responseStream.close();
        getResponse.close();
        postClient.close();

    } catch (IllegalStateException e)
    {
        //handle error
        e.printStackTrace();
    } catch (IOException e)
    {
        //handle error
        e.printStackTrace();
    }
}
```

### HTTP POST
HTTP Post is what's used to start an integration that requires input. Here is a java method that will can post json data to a snaplogic URI, (or any URI with basic auth). To use this method you will need the following libraries in your project. Apache HTTPClient 4.x.x +, Apache HTTPCore 4.x.x +, and Apache Commons-Codec 1.9+.

```java
/*This method takes a json document as a string and posts it to the given URI with the credentials and parameters provided
 * This method accepts parameters for the URI in string form of param=value separated by ;
 *Ex param1=value;param2=value;param3=value
 *dwhite@snaplogic.com
 */
public void doPost(String uname, String pword, String jsonData, String URI, String params, int followRedirect)
{
    CloseableHttpClient postClient = null;
    CloseableHttpResponse postResponse = null;
    HttpUriRequest postRequest = null;
    BufferedReader responseStream = null;
    StringEntity postData = null;
    String b64Creds = null;
    String inputLine = null;
    String[] tempSplit;
    String[] postParams;


    if(followRedirect == 1)
    {
        //Create a new closeable http client that will follow redirects
        postClient = HttpClientBuilder.create().setRedirectStrategy(new LaxRedirectStrategy()).build();

    }else
    {
        //Create a new closeable http client with default settings
        postClient = HttpClientBuilder.create().build();
    }


    //Create your post request with the given URI
    //Start with a request builder
    RequestBuilder rb = RequestBuilder.post().setUri(URI);

    //Now, split and parse the parameter values
    //First, split the incoming string by ;
    postParams = params.split(";");

    //Iterate over every parameter in our newly formed array and add them to our request builder
    for(String d: postParams)
    {
        //Use the empty temporary array to split the parameter name/value pairs apart
        tempSplit = d.split("=");

        //If there aren't two items in the temporary array (name,value), then we don't wan't anything to do with it
        //go on to the next set or end.
        if(tempSplit.length < 2)
        {
            continue;
        }

        //Finally, add the name/value pairs from the temp array to the request builder
        rb.addParameter(tempSplit[0], tempSplit[1]);
    }


    //Add headers to your requestBuilder, for posting JSON to SnapLogic, we want content type and authorization

    //Concatenate and base64 encode your credentials for the header
    b64Creds = Base64.encodeBase64String(StringUtils.getBytesUtf8(uname + ":" + pword));

    //Add your authoization header to the request
    rb.addHeader("Authorization", "basic " + b64Creds);

    //set your content type header, this would change if you were posting XML instead of JSON, SnapLogic natively operates on JSON.
    rb.addHeader("Content-Type", "application/json");


    //set your post payload, in this case, its a JSON document as a string
    //First create your HttpEntity
    try
    {
        postData = new StringEntity(jsonData);
    } catch (UnsupportedEncodingException e)
    {
        //handle error
        e.printStackTrace();
    }

    //Then set it in the request builder
    rb.setEntity(postData);


    //Use the request builder to complete forming the request
    postRequest = rb.build();

    //Now setup is done, execute your post with the HttpClient
    try
    {
        postResponse = postClient.execute(postRequest);
    } catch (ClientProtocolException e)
    {
        //handle error
        e.printStackTrace();
    } catch (IOException e)
    {
        //handle error
        e.printStackTrace();
    }

    //Now we need to parse the response
    //Let's start with the headers
    for(Header h: postResponse.getAllHeaders())
    {
        System.out.println(h.getName() + "=" + h.getValue());
    }

    //Now the response data
    try
    {
        //Setup the buffered reader to read from the incoming stream
        responseStream = new BufferedReader(new InputStreamReader(postResponse.getEntity().getContent()));

        //Read the response data line by line
        while ((inputLine = responseStream.readLine()) != null)
        {
            System.out.println(inputLine);
        }

        //Some clean up
        responseStream.close();
        postResponse.close();
        postClient.close();

    } catch (IllegalStateException e)
    {
        //handle error
        e.printStackTrace();
    } catch (IOException e)
    {
        //handle error
        e.printStackTrace();
    }
}
```

***
***
